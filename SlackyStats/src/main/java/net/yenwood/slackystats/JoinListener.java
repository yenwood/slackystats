package net.yenwood.slackystats;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

public class JoinListener implements Listener {
	private Plugin plugin;
	private static final Player[] EMPTY_STRING_ARRAY = new Player[0];

	public JoinListener(Plugin plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		player.sendMessage("Hi!");

		// Remove them from the save list if they're on it, otherwise set metadata to 0.
		if(Arrays.asList(SlackyStats.playerSaveList).contains(player)){
			SlackyStats.playerSaveList.remove(player);
			Bukkit.getServer().broadcastMessage("Player removed from save list.");
		}else{
			// Set data so it's not null.
			SlackyStats.setMetadata(player, "blocksBroken", 0, plugin);
			SlackyStats.setMetadata(player, "blocksPlaced", 0, plugin);
			SlackyStats.setMetadata(player, "walkDistance", (double) 0, plugin);
			SlackyStats.setMetadata(player, "itemsDropped", 0, plugin);
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e){
		Player player = e.getPlayer();
		SlackyStats.playerSaveList.add(player);
		Bukkit.getServer().broadcastMessage("Player added to save list");
	}
	
	
}
