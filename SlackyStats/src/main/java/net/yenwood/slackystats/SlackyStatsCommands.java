package net.yenwood.slackystats;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SlackyStatsCommands implements CommandExecutor {
	private final SlackyStats plugin;
	
	public SlackyStatsCommands(SlackyStats plugin){
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("slacky")) {
			return true;
		}else if(cmd.getName().equalsIgnoreCase("slackystats")&&args[0].equals("setup")){
	        try {
	        	// Load config
	        	int port = plugin.getConfig().getInt("port");
			    String host = plugin.getConfig().getString("host");
			    String username = plugin.getConfig().getString("username");
			    String database = plugin.getConfig().getString("database");
			    String password = plugin.getConfig().getString("password");
			    int saveDelay = plugin.getConfig().getInt("save_delay");
			    String statsTable = plugin.getConfig().getString("statistics_table");
			    
				Class.forName("com.mysql.jdbc.Driver");			
				String url = "jdbc:mysql://" + host + ":" + port + "/" + database;
				PreparedStatement pst = null;
	
			    Connection con = DriverManager.getConnection(url, username, password);
				pst = con.prepareStatement("DROP TABLE IF EXISTS `slacky_" + statsTable + "`");
				pst.executeUpdate();
				pst = con.prepareStatement("CREATE TABLE `slacky_" + statsTable + "` (`id` int(11) NOT NULL UNIQUE AUTO_INCREMENT, uuid varchar(255), blocksBroken bigint, blocksPlaced bigint, walkDistance bigint, itemsDropped bigint, PRIMARY KEY(uuid))");
				pst.executeUpdate();
						
				Bukkit.getServer().broadcastMessage("SlackyStats successfully setup.");
			} catch (SQLException | ClassNotFoundException e1) {
				Bukkit.getServer().broadcastMessage("SlackyStats has encounted an error: " + e1);
			}
			return true;
		}
		
		// No commands found, return false
		return false; 
	}
	
}
