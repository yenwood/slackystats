package net.yenwood.slackystats;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;

public class BlockListener implements Listener {
	private Plugin plugin;

	public BlockListener(Plugin plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		Player player = e.getPlayer();
		int number = (int) SlackyStats.getMetadata(player, "blocksBroken", this.plugin);
		number++;
		SlackyStats.setMetadata(player, "blocksBroken", number, this.plugin);
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		Player player = e.getPlayer();
		int number = (int) SlackyStats.getMetadata(player, "blocksPlaced", this.plugin);
		number++;
		SlackyStats.setMetadata(player, "blocksPlaced", number, this.plugin);
	}
}
