package net.yenwood.slackystats;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

public class PlayerListener implements Listener {
	private Plugin plugin;

	public PlayerListener(Plugin plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player player = e.getPlayer();
		Location from = e.getFrom();
		Location to = e.getTo();
		
		double distance = to.distance(from);
		double oldDistance = (double) SlackyStats.getMetadata(player, "walkDistance", this.plugin);
		double newDistance = (distance + oldDistance);
		SlackyStats.setMetadata(player, "walkDistance", newDistance, this.plugin);
		Bukkit.getServer().broadcastMessage("Player move event triggered. Distance " + distance + "; Old " + oldDistance + "; New " + newDistance);
	}

	@EventHandler
	public void onPlayerItemDrop(PlayerDropItemEvent e){
		Player player = e.getPlayer();
		Item drop = e.getItemDrop();
		int amt = drop.getItemStack().getAmount();
		
		int itemsDropped = (int) SlackyStats.getMetadata(player, "itemsDropped", this.plugin);
		itemsDropped = itemsDropped + amt;
		SlackyStats.setMetadata(player, "itemsDropped", itemsDropped, this.plugin);
	}
}
