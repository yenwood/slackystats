package net.yenwood.slackystats;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class SlackyStats extends JavaPlugin implements Listener {
	private static Object pst;
	public final JoinListener joinlistener = new JoinListener(this); // Joins and leaves
	public final BlockListener blocklistener = new BlockListener(this); // Blocks placed, destroyed, etc
	public final PlayerListener playerlistener = new PlayerListener(this); // Distance walked, items dropped
	private static final SlackyStatsCommands executor = null; // Command executor
	private static SlackyStats plugin;
	
	// Load player list
	public static List<Player> playerSaveList = new ArrayList();

	
	@Override
	public void onEnable(){
		getLogger().info("SlackyStats has been enabled!");
		
		// Load config
		this.saveDefaultConfig();
		int port = this.getConfig().getInt("port");
		String host = this.getConfig().getString("host");
		String username = this.getConfig().getString("username");
		String database = this.getConfig().getString("database");
		String password = this.getConfig().getString("password");
		int saveDelay = this.getConfig().getInt("save_delay");
		String statsTable = this.getConfig().getString("statistics_table");
		Connection con = null;
		
		// Set Executor for /slacky and /slackystats commands.
		this.getCommand("slacky").setExecutor(new SlackyStatsCommands(this));
		this.getCommand("slackystats").setExecutor(new SlackyStatsCommands(this));
		
		// PluginManager
		PluginManager pm = getServer().getPluginManager();
		
		// Register event listeners for things
		pm.registerEvents(this.joinlistener, this);
		pm.registerEvents(this.blocklistener, this);
		pm.registerEvents(this.playerlistener, this);
		
		// Schedule task to save stats every pre-determined amount of ticks, set in config.yml
		BukkitTask task = new SlackyTasks(this, port, host, username, database, password, statsTable).runTaskTimer(this, 1200, 1200);
	}

	@Override
	public void onDisable(){
		getLogger().info("SlackyStats is being disabled.");
	}
	
	// Metadata stuff... will save stats into each player
	public static void setMetadata(Metadatable object, String key, Object value, Plugin plugin) {
		object.setMetadata(key, new FixedMetadataValue(plugin,value));
	}
		 
	public static Object getMetadata(Metadatable object, String key, Plugin plugin) {
		List<MetadataValue> values = object.getMetadata(key);  
		for (MetadataValue value : values) {
			if (value.getOwningPlugin() == plugin) {
				return value.value();
			}
		}
	return null;
	}
}