package net.yenwood.slackystats;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class SlackyTasks extends BukkitRunnable {
 
    private JavaPlugin plugin;
	private int port;
	private String host;
	private String username;
	private String database;
	private String password;
	private String statsTable;
	private static final Player[] EMPTY_STRING_ARRAY = new Player[0];
 
    public SlackyTasks(JavaPlugin plugin, int port, String host, String username, String database, String password, String statsTable) {
        this.plugin = plugin;
        this.port = port;
        this.host = host;
        this.username = username;
        this.database = database;
        this.password = password;
        this.statsTable = statsTable;
    }

	@Override
	public void run() {
		
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement pst = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");			
			String url = "jdbc:mysql://" + host + ":" + port + "/" + database;

            con = DriverManager.getConnection(url, username, password);
		} catch (SQLException | ClassNotFoundException e1) {
			Bukkit.getServer().broadcastMessage("SlackyStats has encounted an error: " + e1);
		}
		
		ArrayList<Player> players = new ArrayList<Player>(Arrays.asList(plugin.getServer().getOnlinePlayers()));

		for(Player player : SlackyStats.playerSaveList){
			// Add offline players to the pending save stats...
			players.add(player);

			Bukkit.getServer().broadcastMessage(player + " has been added to the players list.");
		}
		
		for(Player player : players){
			// Get UUID
			UUID playerUUID = player.getUniqueId();
			String username = player.getName();
			
			// Collect data
	        int blocksBroken = (int) SlackyStats.getMetadata(player, "blocksBroken", this.plugin);
	        int blocksPlaced = (int) SlackyStats.getMetadata(player, "blocksPlaced", this.plugin);
	        double walkDistance = (double) SlackyStats.getMetadata(player, "walkDistance", this.plugin);
	        int itemsDropped = (int) SlackyStats.getMetadata(player, "itemsDropped", this.plugin);
	        
	        // Save
			try {
				pst = con.prepareStatement("INSERT INTO `slacky_" + statsTable + "` (uuid, blocksBroken, blocksPlaced, walkDistance, itemsDropped)"
						+ " VALUES ('" + playerUUID + "', '" + blocksBroken + "', '" + blocksPlaced + "', '" + walkDistance + "', '" + itemsDropped + "')"
						+ " ON DUPLICATE KEY UPDATE blocksBroken=`blocksBroken` + " + blocksBroken + ", blocksPlaced=`blocksPlaced` + " + blocksPlaced + ","
						+ " walkDistance=`walkDistance` + " + walkDistance + ", itemsDropped=`itemsDropped` + " + itemsDropped + "");
				pst.executeUpdate(); 
				SlackyStats.setMetadata(player, "blocksBroken", 0, plugin);
				SlackyStats.setMetadata(player, "blocksPlaced", 0, plugin);
				SlackyStats.setMetadata(player, "walkDistance", (double) 0, plugin);
				SlackyStats.setMetadata(player, "itemsDropped", 0, plugin);
				
				// Remove them from the save list if they're on it
				if(SlackyStats.playerSaveList!=null){
					if(players.contains(player)){
						SlackyStats.playerSaveList.remove(player);
					}
				}

				Bukkit.getServer().broadcastMessage("SlackyStats has saved the data of all players.");
			}catch (SQLException e) {
				Bukkit.getServer().broadcastMessage("SlackyStats has encounted an error: " + e);
			}
		}
	}

}
